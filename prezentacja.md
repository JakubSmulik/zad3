---
author: Jakub Smulik
title: Grudziądz
subtitle: Prezentacja
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}	
	\usepackage{longtable}
---

# Slajd 1: Informacje o mieście Grudziądz 

​	Miasto położone w województwie Kujawsko-Pomorskim, na prawy brzegu Wisły. Jest czwartym co do wielkości miastem województwa, z liczbą mieszkańców na poziomie 94 359, przy gęstości zaludnienia 1591,6 os./km². Miasto zostało założone 11 kwietnia 1065 roku, a prawa miejskie otrzymało 18 czerwca 1291 roku. 

Flagą Grudziądza jest prostokąt podzielony na kolory: żółty, czerwony biały. Herb przedstawia zamek na murach którego stoi biskup w złotych szatach.  Jednym z symboli miasta jest zamek krzyżacki założony w XIII wieku.  

**Grudziądz jest najmniejszym miastem w Polsce posiadającym własną sieć tramwajową.**

*Poza tramwajami, komunikacja miejska w Grudziądzu dysponuje również autobusami*

\textcolor{red}{Komunikację tramwajową obsługuje jedna linia}, \textcolor{blue}{a komunikację autobusową 24 linie.}

\begin{center} 

Grudziądzka linia tramwajowa jest jedną z najbardziej malowniczych w Europie 

\end{center}

# Slajd 2: Najważniejsze zabytki

Grudziądz znajduje się na Europejskim Szlaku Gotyku Ceglanego. Wśród zabytków można wymienić: 

1. Układ starego miasta 
2. Mury miejskie 
3. Zamek krzyżacki 
4. Zabudowa starego miasta 
5. Cytadela 

Do najważniejszych zabytków sakralnych należą: 

- Klasztor Benedyktynek 
- Kościół Ewangelicki 
- Kościół Garnizonowy 
- Bazylika Kolegiacka Św Mikołaja 
- Kościół Św Jana 

Dodatkowo ciekawymi turystycznymi obiektami są: 


- Obiekty użytkowe 
  - Stara zajezdnia tramwajowa 
  - Biblioteka miejska 
  - Mały ratusz 

- Obiekty przemysłowe 
  - Dawna fabryka maszyn A. VENTZKI
  - Górny młyn 
  - Ruiny browaru Grudziądzkiego 
- Obiekty zielone 
  - Park miejski im Piotra Janowskiego 
  - Ogród botaniczny 

# Slajd 3: Wspólnoty wyznaniowe

### Największa wspólnota w Grudziądzu

Kościół rzymsko-katolicki 

### 

Cerkiew prawosławna 

###

Kościół protestancki 

###

Świadkowie Jehowy 

###

Buddyjski Związek Diamentowej Drogi Linii Karma Kagyu

# Slajd 4: Garnizony wojskowe w Grudziądzu 

\begin{alertblock}{Jednowstki stacjonujące obecnie}
Obecnie w Grudziądzu stacjonuje 5 jednostek wojskowych.
\end{alertblock}

27 września 2007 roku odbyły się w Grudziądzu manewry wojskowe Opal 07

\begin{block}{Jednostki stacjonujące po 1945 roku, rozwiązane}
Po 45 roku w Grudziądzu stacjonowało 10 jednostek wojskowych.
\end{block}

\begin{exampleblock}{Jednostki stacjonujące przed 1939}
Przed 39 rokiem w Grudziądzu stacjonowało 9 jednostek wojskowych.
\end{exampleblock}

# Slajd 5: Mity związane z Grudziądzem

::: {.columns}
:::: {.column width=0.3}
Legenda mówi, że przez 7 lat oblężania zamku, Szwedzi chcieli zagłodzić jego załogę. Mieszkańcy mieli się poddać jednak pewien mieszczanin wpadł na pomysł. Wystrzelono łeb woła i kawałek chleba w pozycję Szwedów. Szwedzi widząc że mają duże zapasy żywności, odstąpili od oblężenia zamku.
::::
:::: {.column width=0.3}
W czasie szwedzkiej okupacji pewna mieszkanka pokochała Szweda. Planowali uciec razem do Szwecji, jednak ojciec dziewczyny się o tym dowiedział i kazał zamordować Szweda. Przybita wrzuciła wszystkie kosztowności do studni i wstąpiła do klasztoru. Do dzisiaj nie udało się ich wydobyć.
::::
:::: {.column width=0.3}
Mikołaj Kopernik słysząc o problemach Grudziądza z wodą pitną, postanowił im pomóc. Zaproponował wykopanie kanału z rzeki Osy do miasta. Kanał Trynka został oddany do użytku w 1552 roku. 
::::
:::



# Slajd 6: Panorama miasta 

![](grudziądz.jpg)

# Slajd 7: Miasta partnerskie 



| Miasto |   Kraj   |   Data podpisania umowy   |
| --------- | ---- | ---- |
| Gütersloh | Niemcy | 1989 |
| Falun | Szwecja | 1991 |
| Czerniachowsk | Rosja | 1995 |
| Nanning | Chiny | 2011 |

